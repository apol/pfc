\documentclass[a4paper,10pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage{url}
\usepackage{graphicx}
\usepackage{multicol}
% \usepackage{fullpage} %margins

%opening
\title{Static Analysis on Imperative Languages}
\author{Aleix Pol Gonzàlez\\
  Tutor: José Miguel Rivero Almeida
}

\setcounter{tocdepth}{3}

\begin{document}

\maketitle

\begin{abstract}
In this document we describe what work is being done on adding static analysis capabilities to a language platform like KDevelop's architecture and how we turned it into a working project.
These static capabilities will have in mind the regular imperative and object oriented languages and will use C++ as the provided implementation.
\end{abstract}

\newpage
\tableofcontents
\newpage

\section{Introduction}
this project's central idea is to create a framework that will make it possible to analyze source code and report relevant information the best possible way. To do so, we split the project into 3 main parts: the data recollection, the check development, and a number of ways to present the conclusions to the user. Likewise this document has been prepared in the same way.

This project, even if it could be adapted to many languages, is focuses C++ analyzing because: On the one hand, it's an important and widespread language that could make good use of the project right away. On the other hand, we have a working implementation of it in KDevelop, which narrows down our work to extending the analysis process to actually do what we're looking for, saving us the non-trivial task of designing and developing a C++ parser.

	% this paragraph is ONE (too long) sentence
	% take advantage = different meaning!

\subsection{Why static analysis?}
When I was thinking about choosing a subject for this project I knew I wanted to work on programming languages. Moreover I wanted to create something that would help developers create better software. My KDevelop background was helpful when I decided to start creating tools that would end up in this framework creation.

First of all I wanted to start a project that would take advantage of the amazing KDevPlatform infrastructure to create checks. There are (only) few static analysis tools in the Free Software world and my idea was that we could make a difference there.

This was why I decided to create a tool that will find certain problems in random software projects. To do so we started to create the checking infrastructure and I began to work on creating checks that point out the most simplistic problems that a compiler generally won't.

Using this project we want to achieve simplicity with regard to the check development. It should be relatively easy to create new checks which make sure that trivial problems don't arise and support some of the not-so-trivial ones.

\subsection{Why Free Software?}
% REVIEW
Given the nature of this project, we believe that having the most freedom on the code base is the best approach.

We have created a project that should keep alive by two means: all projects interested in it should be able to use it and developers interested in taking it further should be able to do so, with the most ease possible.

That's why the software provided by this project is going to be released under the LGPL license together with the rest of KDevPlatform and KDevelop.

\subsection{The Starting Point}
As we have mentioned before, this project is largely built on top of the KDevelop project which was already working when the current project started. In KDevelop we could find some abstractions already created, like the plugin infrastructure or facilities to ask the platform to parse a project and tell the language support to parse all its files. What we did was to add the necessary changes so that we would get the files parsed the way we want them to be (by default KDevelop does some minimal parsing on those files that are not being edited, which is not sufficient for our purposes) and of course to add the new interfaces and provide them from the C++ support.

\subsection{Goals}
Here the project's main objectives are summarised:
\begin{itemize}
  \item To abstract the language support from the check creation. The check developer doesn't have to be responsible for the modification of AST\footnote{Stands for Abstract Syntax Tree, it's the code representation provided by the parser.} visitors to check the information, but gather the data once and then have it provided.
  \item To provide a set of checks as minimal features and as a proof of concept.
  \item To provide a set of tools to display the checks findings in a user friendly way.
\end{itemize}

\section{Architecture and Data Collection}
First of all, to be able to come to conclusions regarding some code, we will extract the data we need and we will put it in abstract interfaces. An alternative method would be to provide the raw AST, but I believe that this way we will be able to get more powerful checks with less effort. For the moment, we will be providing 3 main data sources: the DUChain, a Control Flow Graph, and information about the data access for every variable in the code. I believe that based on this information a wide range of problems can be addressed already.

Regarding the DUChain, even though it's not provided by this project (it was in KDevPlatform already) I will try to explain it briefly so that the reader will understand why is it useful. DUChain is short for Declaration-Use Chain and its main purpose is to create a structured database with all the code symbols and their relevant information. Originally the idea behind it was to provide a solid base to create code completion and code navigation on top of the raw parser information. However, it has proved to be a very useful information repository that can be useful in different cases. In the DUChain we will store any variable or type declaration together with their type and uses. This makes it possible to analyze it and provides much useful information. More information about DUChain can be found in KDevPlatform's DUChain Documentation. \cite{apikdeorg}

\subsection{Interfaces}
In the following section I will explain the interfaces that have been created for both, accessing and constructing the information. A more detailed presentation of the design of the DUChain would be beyond the scope of this project and will thus be left out.

\subsection{Data Access}
This interface will tell us for every variable if it's being read, modified, or both. By variable here, we mean an identifier representing some data. We will be able to extract the information where the data comes from by accessing the position's Declaration or Use.

\subsubsection{Implementation}
There are two main Objects that we are going to use to deal with all this: the DataAccess and the DataAccessRepository.

	% "all this" ?

\begin{verbatim}
class DataAccess
{
    public:
        enum DataAccessFlag { None=0, Read=1, Write=2, Call=4 };
        
        bool isRead()  const;
        bool isWrite() const;
        
        RangeInRevision valueRange() const;
        CursorInRevision pos() const;
        DataAccessFlags flags() const;
};
\end{verbatim}

The class DataAccess is used to identify every access to any symbol provided by the code. All of those contain a \textit{CursorInRevision} instance that can be used to relate it to its position in the code (and also to its corresponding Declaration instance), also a valueRange that, in case it's a Write and it's possible, will tell us which expression is responsible for the variable's write, and finally also the flags that will tell us what type of data access it is, either a Read, Write or Call.

\begin{verbatim}
class DataAccessRepository
{
public:
  void addModification(CursorInRevision cursor,
                       DataAccess::DataAccessFlags flags);
    
  void clear();
  QList<DataAccess*> modifications() const;
  DataAccess* accessAt(CursorInRevision cursor) const;
  QList<DataAccess*> accessesInRange(RangeInRevision range) const;
};
\end{verbatim}

The DataAccessRepository is the code structure we will use to access every data access in a source file. Here we are providing 3 methods to access: one that retrieves all accesses at once, another that given a code position returns the DataAccess there, if any, and finally a third that provides all the accesses inside a range.

\subsubsection{Use cases}
The use case for the data access data structure is rather simple. Especially in the case of imperative languages (the ones treated by this project), it's very useful to understand where we are accessing data and where we are modifying it. It's a very common problem if we're adding code that changes the value semantics in unnecessary ways. We need to be able to know where values are coming from and what is being changed. Together with the Control Flow Graph we can predict how the program will evolve in the future.

\subsection{Control Flow Graph}
The other data interface that we have introduced is the Control Flow Graph. It is used to create a representation of how the code is going to behave considering a current state. This is achieved by specifying what the different code paths are and which parts tell who decide what's the alternative path to take.


\begin{multicols}{2}
\begin{verbatim}
int f(int a)
{
  if(a)
    a+=1;
  else
    a-=1;
  return a+3;
}
\end{verbatim}

\includegraphics[width=5cm]{controlflow.eps}
\end{multicols} 

Here we can see the output\footnote{As will be discussed later, this graph is automatically generated by the unit tests} of the code placed on the left hand side in form of a control flow graph in the figure on the right hand side. We can see that instead of having the code in a form of text array the relationships regarding the code flow are visualized.

\subsubsection{Implementation}
We have 2 important classes to know about regarding the Control Flow infrastructure:

\begin{verbatim}
class ControlFlowNode
{
  public:
    enum Type { Conditional, Sequential, Exit };
    
    Type type() const;
    
    void setStartCursor(CursorInRevision cursor);
    void setEndCursor(CursorInRevision cursor);
    
    RangeInRevision nodeRange() const;
    RangeInRevision conditionRange() const;
    
    ControlFlowNode* next() const;
    ControlFlowNode* alternative() const;
};
\end{verbatim} 

The ControlFlowNode class will be used to specify one code chunk that will always be executed sequentially as a block. Here we're providing different information:
\begin{itemize}
  \item The node type that will tell us if it has 1, 2, or no following nodes. 
    \begin{itemize}
      \item If it has just one it will be sequential; it will inevitably move on to the next one.
      \item If it has both this means that it's a conditional. In this case the conditionRange will be specified.
      \item Otherwise it's an Exit, which means that it's an end of the current code path.
    \end{itemize}
  \item The nodeRange that will store the whole node range.
  \item The conditionRange will return a range that will specify which range will directly dictate which of the 2 paths to take.
  \item The next and alternative property will provide us with the different nodes that will follow the current node depending on the current state.
\end{itemize}

\begin{verbatim}
class ControlFlowGraph
{
public:
  void addEntry(ControlFlowNode* n);
  void addEntry(Declaration* d,
                ControlFlowNode* n);
  void addDeadNode(ControlFlowNode* n);
  void clear();
  
  QList<Declaration*> declarations() const;
  ControlFlowNode* nodePerDeclaration(Declaration* d);
  
  QList<ControlFlowNode*> graphNodes() const;
  QVector<ControlFlowNode*> deadNodes() const;
};
\end{verbatim}

The ControlFlowGraph is, like the DataAccessRepository, the class that will store every file's gathered information and will provide easy ways to access it from the checks or from wherever it will be needed.

In this class we will be providing 3 methods to feed nodes into it: addEntry will let us add a random node found in the code, addEntry with a symbol Declaration (we mostly expect functions and variables definitions here), that will add it and additionally will link the Declaration to the node, so that we can know what it is doing and when it is going to be called. Finally we also have the addDeadNode method that will add the node just like the plain addEntry but will save it as a dead node. These dead nodes are ones that will never be run.

\subsubsection{Use cases}
If we want to be able to analyze some code base we need to know what the code is going to react to and where it will lead to. The Control Flow Graph tackles that by providing us with the network of states that could happen in it.

A typical use case example would be the constant condition problem, where we get to statically tell that a condition is always going to jump to the same place, which would mean that it's an unneeded condition that can be removed.

\subsection{Data Structures' Implementation for C++}
To implement both of these interfaces in the C++ support we added two new AST walkers that understand the code structure and provide the data we are looking for. This way, as we exposed before, we will not need to link (or even understand) the language implementation. 

First of all the Data Access interface was implemented. At first glance it appeared to be easier to implement than the Control Flow Graph. Eventually this was not so, although the difficulties encountered arose due to some initial design errors.
The implemented algorithm is quite simple. As we said, it's a typical AST visitor. Here we want to track to what type the current variable is being fed. In case it's a reference the variable will be read and written. Otherwise the reference will be read only. With this idea in mind we keep tracking what argument we're in and in the cases the type is built-in by the language we also generate our own parameter types.
For the write-only types like definition and assign this will be done right away so that we can properly provide a valueRange.

The other implemented interface is the Control Flow Graph. The resulting implementation was quite forward -- like the previous one this is an AST visitor. However in this case we: 
\begin{itemize}
\item Wait to find a code context (either a declaration or a function/method definition) 
\item There we create the first node in the graph. We walk into the code node recursively and finally we feed it back to the graph together with its declaration if possible.
\item While walking the expression node we will find different conditional structures such as: if's, while's, or for's. When we want to create a new node we will proceed as follows: 
	\begin{itemize}
	\item we set the ending cursor of the node. 
	\item we create a new node with the appropriate start cursor. 
	\item we link it to the previous node and proceed the visiting.
	\end{itemize}
\end{itemize}


All this has to be done in a sufficiently reliable way. That's why we created some unit tests for both interfaces. This way we can ensure that everything works as expected. 

The data access interface has a test that provides some code and a list of DataAccess::Flags values. We parse and analyze the code and compare the results to what we expected, like we do the same with the Control Flow Graph's implementation, but this time we just pass the tested code and the number of nodes. This is already quite significant check if there have been any regressions. Given that by using this approach we're not taking into account the ranges, when we run the code a graphviz file \cite{graphviz} is generated for every test case so that we can review its consistency ourselves.

\section{Checking Infrastructure}
Once we have all the data we have to start thinking about what a check is and about how far we want to take them. The KDevPlatform semantics are rich enough regarding problem generation but we want to define what we're aiming for, so that we can act accordingly in the future. Furthermore, we don't want to exclude any possibilities. We tried to make a flexible interface that fits our needs but we don't make sure it's used entirely the way we expect it to.

Given the scope of this project, we can consider a check as a program that given some data: adds problem items to the project, tying them to its file. In this chapter we will explore what limitations and possibilities this approach offers.

\subsection{Check Interfaces}
When we start thinking about implementing a check, the first thing we will do is to consider how we achieve that our code is triggered at the correct time, so that we can start thinking about what approach to take. We don't want check developers to wonder about all these problems so we will try to solve it once in the platform and let check developers work on their own issues.

To do so, firstly, we will define the interface which the check developer will implement. It's called ILanguageCheck interface and looks like the following:

\begin{verbatim}
class ILanguageCheck
{
public:
  virtual QString name() const = 0;
  virtual void runCheck(CheckData data) = 0;
};
\end{verbatim}

Here we are requiring to implement two things: to provide a name, in order to have some identification for the check for UI usability and debugging purposes, and also the important method, the runCheck. This is the one that will be called whenever we need the check to start analyzing the data and, in case it's needed, to provide the problems the check finds.

In case we want to provide different checks from a plugin we will also have the ILanguageCheckProvider:

\begin{verbatim}
class ILanguageCheckProvider
{
public:
  virtual QList<ILanguageCheck*> providedChecks() = 0;
};
\end{verbatim} 

This one will provide different ILanguageCheck instances just like those explained above which will be implemented in the same way.

\begin{verbatim}
class CheckData
{
public:
  KUrl url() const;
  TopDUContext* top() const;
  ControlFlowGraph* flow() const;
  DataAccessRepository* access() const;
};
\end{verbatim}

Whenever we need to run the code, we will receive a CheckData class that will provide the required information. For the moment we will be getting the file location in a KUrl form, the file's TopDUContext to access the DUChain data, the file's ControlFlowGraph to know about its code paths and the file's DataAccess's information to know how the program is dealing with the data. We hope it will be possible to create a great number of checks with this.

\section{Check Execution}
Now that we have reviewed our check architecture we need different pieces to make it possible to make full use of the provided checks. In this chapter we will explain the tools that have been created in order to do so.

\subsection{Running Checks}

	% missing paragraph

\subsubsection{Editor integration}
The first chosen approach was to create a KDevelop plugin, mainly for the sake of simplicity. In KDevelop we already have most of the environment logic prepared: we are importing the projects, we are parsing the project's files, we have places in the GUI where to show the found problems. In this case what we did was to prepare a plugin that hooks to the KDevelop GUI and there performs the checking when it's due. 
This means we are getting automatically the checks being run while developing KDevelop. It's not our final use case but I think it's a very interesting feature.

\subsubsection{Project checking}
We also want to have a standalone application that parses and analyzes a given project and then provides a report with all problems found in the project. 

\subsection{Check Distribution}
When starting to develop checks, the first thing that occurred to me in order to start developing checks, was to start the checks pack. Here we have a plugin that is responsible for the infrastructure by providing every check inside the pack, which will be in a separate .cpp file that statically registers itself to the plugin to be initialized when it's required.

More on the subject will follow focusing on how we can improve the check distribution in a general and efficient way.

\section{Creating checks}
When we were talking about the project's goals, we said that one of the important parts is to make it easy to create further checks. In this chapter we will explain (what) the proposed way to create checks (is), what we expect the developer to do, and what the developer will have to worry about. In this document we have specified how to create a wide range of opportunities for the check developers, here we will try to narrow these possibilities down in favor of flexibility.

\subsection{C++ checks}
Creating C++ checks is a very good option if we want to be aware of how the check code will be executed when it's working on a computer, but that is a very specific case and it isn't always what we are looking for. Sometimes we will want to favor ease of distribution or code simplicity instead of run-time efficiency. That's why we have created this section, where we will discuss the advantages and problems of every option.

\subsection{Scripted Checks}
C++ checks are great, lets us to use the KDevPlatform library as it was meant. We can also be very specific about what we want the check to do so that it's efficient. And we can do whatever we feel like with it, it's a KDevelop plugin and our only limitation is our creativity. On the other hand we found some limitations that we consider worth addressing.

First of all in the C++ code we're defining how to check something but, in a way, we're going for what to check. This means that sometimes writing those checks can be a little hard in C++. In addition there's the distribution problem, a C++ check has to be compiled before installing and, given the different places where it can be run, it's probably a good idea to be able to have an alternative.

\subsubsection{Choosing a language, Alternatives}
I will try to list what requirements I wanted, once we decided to look for an alternative language to define checks, before I will turn to the actual decision.

As we have said before, the problem we're dealing with here is pretty well defined: we have a bunch of data and we want to turn it into a set of problems. We need a language that is great when dealing with various data structures. Neither IO, nor threading, nor UI, etc. This definition works very well if we think about the functional paradigm. Moreover, we want to remove the compilation requirement, so scripted sounds good, too.

\begin{itemize}
	\item Prolog was the first language we considered. It makes complete sense because it's aimed at data analysis and problem solving. The issue there was that it looked quite hard because most C++ bindings I found required us to feed all the data to Prolog. Our conclusion was that, given that there's a lot of information already fetched in C++ data types, it might not be the best idea to use it. If that was a wrong premise, this could lead to a very interesting research project in the future.
	\item Haskell seemed eligible, too. Its pure functional point of view would have been great for our needs, the main problem here was that its code requires compilation. Also there's some problems that we wouldn't be able to solve with the default \textit{c2hk} tool being mostly targeting C instead of C++, so we would need to create bindings for those anyway. 
	\item Python and Ruby were considered as well. Since their paradigms are quite similar I will treat them together. I quite liked both because of their scripting nature; they both are very scriptable even if they are not really intended to serve as plugin languages, but as full applications bindings. Both require quite some learning and effort to create the bindings. The main problem with them is that there's no such concept for type checking which would be very helpful for solving this kind of problem (turning a lot of different data into problems). The lack of type checking was considered very bad in this case, as we want to make sure the checks are consistent. In a project like this one it makes sense to use languages that can be statically checked.
	\item JavaScript is a very good candidate as well. It's very easy to integrate with the KDevPlatform because Qt \footnote{The Qt Project is the library KDevelop and KDevPlatform are based on.} already provides ways to support JavaScript scripts from our applications (not so) easily. While it's possible to feed classes ,they (also) have to be QObject based. This is fine, yet not great because it would require the creation of bindings. In addition there's the problem that there's no type checking whatsoever.
	\item KAlgebra's Analitza module was considered mainly because it's being developed by me. The language was initially created to support a calculator. Over the course of time it has got some advanced features like static type checking with type inference and some speed improvements that made it worth considering. It is scripted, we can run its scripts by simply linking against a library and feeding the source and bindings there, it felt good. It has some of the advantages and disadvantages of most languages listed here, but having developed it for several years also helps to make sure that we're getting the most out of the language, even if it required many additions to the KAlgebra language.
\end{itemize}

\subsubsection{Adapting the language}
As I said, even if it made sense to adapt the KAlgebra language, it was missing some very basic features like string support, built-in methods (or bindings if you prefer) and abstracting a little the way we have to import.

¿...? \footnote{Remove section altogether?}

\subsubsection{Binding to the language}
To create the KDevPlatform bindings we needed an easy way to create a bunch of functions that communicate our scripts to the KDevPlatform without spending too much time on the task since it's not the central objective of this project. To this end we decided to go for cpptoxml. It was very convenient because it gets a C++ file and it automatically outputs an XML document. It's really easy to read XML (there's a lot of technology already created for that), so with it we created an application that: given a header file and which classes we want to extract from it, it extracts them to be analyzed. From here we will extract all the methods and create a function from one of those. We will also read every enumeration and will create values for whatever we find there. It should be noted that for every method we will have to check all the arguments and in case it's something that KAlgebra understands we have to translate it to its corresponding KAlgebra type (like in numbers, lists, strings, etc.). If it doesn't exist we can create a KAlgebra custom object value that KAlgebra treats as if it were a black box, which can't be read, but passed around only.

\section{Checks}
As we said, this project will provide a set of checks that will show us, on the one hand, how to make use of the facilities provided by this project and, on the other, will start to provide some features that we will be able to use in our projects.

All these checks have been developed using the technologies created in this project, mainly the new data structures and the language that was adapted especially for this purpose, as evidence that it works fine and also that it's useful to create any kind of check.

\subsection{Imports}
This is the first check that was implemented using this infrastructure. It was decided to start with this one because it depends on the DUChain only, which is convenient as a first approach.
This one gathers all the Declarations used in the top context (that don't come from the same context) and checks if there's any import importing it. It's a good check because it helps to keep track of which imports are good and which ones aren't.

\subsection{Const Method Check}
This check is also relying on DUChain only and it tells us if any class method is not modifying any class attribute and in this case it adds an error telling that the \texttt{const} is missing.

\subsection{Unused Declaration Check}
For every declaration in the file we check if it's being used (with some practical exceptions) and if there isn't then we report it.

\subsection{Consecutive}
For every use we check several things that will tell us if it's properly placed by analyzing if there \textbf{have} been two following writes or a read without any previous writes only.

\section{Conclusions}
...
\begin{itemize}
  \item KDevPlatform clone with the modifications.
    \url{http://gitweb.kde.org/?p=clones/kdevplatform/apol/kdevplatform-pfc.git}
  \item KDevelop clone with the modifications.
    \url{http://gitweb.kde.org/?p=clones/kdevelop/apol/kdevelop-pfc.git}
  \item KDevChecksRunner. The plugin responsible for the check invocation.
    \url{http://gitweb.kde.org/?p=scratch/apol/kdevchecksrunner.git}
  \item KDevChecksPack. A pack for C++ checks.
    \url{http://gitweb.kde.org/?p=scratch/apol/kdevcheckspack.git}
  \item KDevAnalitzaChecks. A plugin that provides the bindings to create Analitza checks (the language discussed before)
    \url{http://gitweb.kde.org/?p=scratch/apol/kdevanalitzachecks.git}
\end{itemize}

In these URL's you'll find some websites that will tell you how to retrieve the code. In case you want to try it out, you'll have to compile it like regular KDevelop. Instructions can be found here: \url{http://techbase.kde.org/KDevelop4/HowToCompile}, of course by using the repositories provided by this project.


\begin{thebibliography}{9}

\bibitem{apikdeorg} KDevPlatform DUChain Documentation, available at
\url{http://api.kde.org/extragear-api/kdevelop-apidocs/}.
\bibitem{kde} The KDE community
  \url{http://kde.org}.
\bibitem{graphviz} Graphviz is open source graph visualization software.
\url{http://www.graphviz.org/}.
\end{thebibliography}

\end{document}
  