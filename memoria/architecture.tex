\chapter{Architecture and Data Collection}
First of all, to be able to come to conclusions regarding some code, we will extract the data we need and we will put it in abstract interfaces. An alternative method would be to provide the raw AST, but I believe that this way we will be able to get more powerful checks with less effort. For the moment, we will be providing 3 main data sources: the DUChain, a Control Flow Graph, and information about the data access for every variable in the code. I believe that, based on this information, a wide range of problems can already be addressed.

Regarding the DUChain, even though it's not provided by this project (it already was in KDevPlatform) I will try to explain it briefly so that the reader will understand why is it useful. DUChain is short for Declaration-Use Chain and its main purpose is to create a structured database with all the code symbols and their relevant information. Originally the idea behind it was to provide a solid base to create code completion and code navigation on top of the raw parser information. However, it has proved to be a very useful information repository that can be useful in different cases. In the DUChain we will store any variable or type declaration together with their type and uses. This makes it possible to analyze it and extract very useful information.

\begin{figure}[h]
  \includegraphics[width=300px]{./duchain.png}
  \centering
  \caption{KDevelop's graphical representation of the DUChain.}
\end{figure}

In this picture we can see some features from the DUChain that are being used in the KDevelop editor. When hovering a variable use, we're highlighting the declaration and all the uses for the symbol and also we're showing a tool tip window with information relevant to the variable declaration. As we said, we have everything indexed and we can navigate it to find whatever we need.

More information about DUChain can be found in KDevPlatform's DUChain Documentation \cite{apikdeorg}.

Before diving into the interfaces, I'd like to put in words why we are making such a separation between the code back-end and the checks. It's arguable that we need to provide such abstractions and it wouldn't be fair to you, the reader, that I didn't comment on it. Code analysis is not a simple process, there are different steps involved and we will be most likely require all of them in some of the checks we are using. In this project we are going for creating an infrastructure that eases the use of a lot of small checks.
For the moment, I'm not interested in being able to build a big piece of code that does everything, we want to provide tools that fit any check developers' needs. If we are missing something, then the missing part should contributed again to a common place, where all checks can access. We want to unleash the possible check contributors' creativity. I believe this is reachable by providing diverse, useful and well-defined resources, more than leaving space for implementation freedom, such as the AST exposition, because it could lead to similar implementations on every check. We want to reuse code, we want every tool we make to add some value to the next.

All check interfaces are language independent theoretically, but in practice checks might not be cross language. This is fine, because the important thing is that just by knowing how the language works the knowledge transfer, so does the documentation, so when implementing checks for a new language we will just need to provide the abstractions and make sure what translates and whatnot.

\section{Interfaces}
In the following section I will explain the interfaces that have been created for both, accessing and constructing the information.

During this project an important amount of time was dedicated to the interface designing. The power of our future checks depend on how flexible those can be so, first of all, let's define them carefully.

\section{Data Access}
This interface will tell us for every variable if it's being read, modified, or both. By variable here, I mean an identifier representing some data. Therefore, we will be able to extract the information where the data comes from by accessing the position's Declaration or Use.

\subsection{Implementation}
There are two main objects that we are going to use to access and store the Data Access information: the DataAccess and the DataAccessRepository.

\begin{verbatim}
class DataAccess
{
    public:
        enum DataAccessFlag { None=0, Read=1, Write=2, Call=4 };
        
        bool isRead()  const;
        bool isWrite() const;
        
        RangeInRevision valueRange() const;
        CursorInRevision pos() const;
        DataAccessFlags flags() const;
};
\end{verbatim}

The class DataAccess is used to identify every one access to any identifier provided in the code. All of those contain a \textit{CursorInRevision} instance that can be used to relate it to its position in the code (and also to its corresponding Declaration instance), also a valueRange that, in case it's a Write and it's possible, will tell us which expression is responsible for the variable's write, and finally also the flags that will tell us what type of data access it is, either a Read, Write or Call.

It should be noted that when we are talking about cursor and ranges, we are talking about code positions. The cursor and range abstractions are provided by the KDevPlatform through RangeInRevision and CursorInRevision. A cursor will be, mainly, a line in a column that, together with the URL, will specify a position in some code. A range is like a constrained pair of two cursors: one pointing to the start and the other pointing to the end of the range.

\begin{verbatim}
class DataAccessRepository
{
public:
  void addModification(CursorInRevision cursor,
                       DataAccess::DataAccessFlags flags);
    
  void clear();
  QList<DataAccess*> modifications() const;
  DataAccess* accessAt(CursorInRevision cursor) const;
  QList<DataAccess*> accessesInRange(RangeInRevision range) const;
};
\end{verbatim}

The DataAccessRepository is the code structure we will use to retrieve data access information in a source file. We are providing 3 methods to retrieve this data:

\begin{description}
  \item[modifications] This method will return a list of all data accesses found in the repository's file in the order that the language implementation provided it.
  \item[accessAt] It will return the data access that we have at the \textit{cursor} position. If there's none, a pointer with 0 value will be returned.
  \item[accessesInRange] It will return all the accesses in the repository that are contained by the \textit{range} argument, just as if filtered the modifications method above with cursor's method \textit{contains}.
\end{description}

To see a little better how it works, I'll provide an example with its expected results:
\begin{verbatim}
int a, b;
int g(int&, int);

int f()
{
  return g(a, b);
}
\end{verbatim}

In this example there are 3 declarations: 2 variables and a \textit{g} function that has two arguments, a reference integer and an integer. Finally there's the \textit{f} function that just calls \textit{g} and uses the global variables to call the arguments.

In this code we will find 3 data accesses: 
\begin{description}
 \item[DataAccess::Write|DataAccess::Read] A read/write access for the first function argument, because the function declaration is saying that it's a reference, so this function could be modifying the argument (otherwise it would be \texttt{const}).
 \item[DataAccess::Read] A read for the second function argument.
 \item[DataAccess::Read|DataAccess::Call] A read/call for the function argument name.
\end{description}

With this we should be able to know what kind of data is being accessed and why, more or less. This data will be specially useful together with the Control Flow Graph and the DUChain.

\subsection{Use cases}
The use case for the data access data structure is rather simple. Especially in the case of imperative languages, the ones treated in this project, it's very useful to understand where we are accessing data and where we are modifying it. It's a very common problem to add code that changes the value semantics in unnecessary ways. We need to be able to know where values are coming from and what is being changed. Together with the Control Flow Graph we can predict how the program will evolve in the future.

Another interesting use case for the Data Access structure is the constant conditional case where we have to be able to track where the data is coming from if we want to be able to tell if some data is constant or not along the program.

\section{Control Flow Graph}
The other data interface that we have introduced is the Control Flow Graph. It is used to create a representation of how the code is going to behave considering a current state. This is achieved by specifying what the different code paths are and which parts tell who decide what's the alternative path to take.

This data structure can be abstracted like a graph where every node is a sequential chunk of code that will be run sequentially and every edge is the places where there's some run-time decision to be taken that will decide where does the execution follow.

\begin{multicols}{2}
\begin{verbatim}
int f(int a)
{
  if(a)
    a+=1;
  else
    a-=1;
  return a+3;
}
\end{verbatim}

\includegraphics[width=5cm]{controlflow.eps}
\end{multicols} 

Here we can see the output\footnote{As will be discussed later, this graph is automatically generated by the unit tests} of the code placed on the left hand side in form of a control flow graph in the figure on the right hand side. We can see that instead of having the code in a form of text array the relationships regarding the code flow are visualized.

\subsection{Implementation}
We have 2 important classes to know about regarding the Control Flow infrastructure:

\begin{verbatim}
class ControlFlowNode
{
  public:
    enum Type { Conditional, Sequential, Exit };
    
    Type type() const;
    
    void setStartCursor(CursorInRevision cursor);
    void setEndCursor(CursorInRevision cursor);
    
    RangeInRevision nodeRange() const;
    RangeInRevision conditionRange() const;
    
    ControlFlowNode* next() const;
    ControlFlowNode* alternative() const;
};
\end{verbatim} 

The ControlFlowNode class will be used to specify one code chunk that will always be executed sequentially as a block. Here we're providing different information:
\begin{description}
  \item[type] tells if the node has 1, 2 or no following nodes. 
    \begin{description}
      \item[Sequential] If it is sequential node, it means that the node just points to 1 node; it will inevitably move on to the next one.
      \item[Conditional] If it is a conditional node, it means it has two following nodes. In this case the \texttt{conditionRange} will be specified which will be responsible for where to go next.
      \item[Exit] If it's an Exit it has no following nodes, which means that it's an end of the current code path.
    \end{description}
  \item[nodeRange] tells the range that defines the node, from its start to its end.
  \item[conditionRange] returns the range that will specify which range will directly dictate which of the 2 paths to take, in case it's a conditional node, otherwise it will return an invalid node.
  \item[next and alternative] properties will provide us with the different nodes that will follow the current node, depending on the current state.
\end{description}

\begin{verbatim}
class ControlFlowGraph
{
public:
  void addEntry(ControlFlowNode* n);
  void addEntry(Declaration* d,
                ControlFlowNode* n);
  void addDeadNode(ControlFlowNode* n);
  void clear();
  
  QList<Declaration*> declarations() const;
  ControlFlowNode* nodePerDeclaration(Declaration* d);
  
  QList<ControlFlowNode*> graphNodes() const;
  QVector<ControlFlowNode*> deadNodes() const;
};
\end{verbatim}

The ControlFlowGraph is, like the DataAccessRepository, the class that will store every file's gathered information and will provide easy ways to access it from the checks or from wherever it will be needed.

In this class we will be providing 3 methods to feed nodes into it: addEntry will let us add a random node found in the code, addEntry with a symbol Declaration (we mostly expect functions and variables definitions here), that will add it and additionally will link the Declaration to the node, so that we can know what it is doing and when it is going to be called. Finally we also have the addDeadNode method that will add the node just like the plain addEntry but will save it as a dead node. These dead nodes are the ones that will never be run.

Before going on with the use cases, let me define a little what is a dead node. Dead nodes are those that will never be called given the language syntax. It doesn't mean that there's no logic that can lead there, but that there's no path that leads there, but still there's the code. Let's see some C++ example:

\begin{verbatim}
int f()
{
  int x=2+sin(pi)/4;
  return x;
  awesome_function();
}
\end{verbatim} 

In this example, there's not even the possibility that \texttt{awesome\_function} is ever called, then we will generate a dead node from right after the return until the end of the function.

\begin{verbatim}
int f()
{
  int x=2+sin(pi)/4;
  for(;;) {}
  awesome_function();
}
\end{verbatim} 

In this example there won't be a dead node because it's not a syntactical reason that tells us that \texttt{awesome\_function} won't be called, therefore we will have to figure it out in some other way.

\subsection{Use cases}
If we want to be able to analyze some code base we need to know what the code is going to react to and where it will lead to. The Control Flow Graph tackles that by providing us with the network of states that could happen in it.

A typical use case example would be the constant condition problem, where we get to statically tell that a condition is always going to jump to the same place, which would mean that it's an unneeded condition that can be removed.

\section{Data Structures' Implementation for C++}
To implement both of these interfaces in the C++ support we added two new AST walkers that understand the code structure and provide the data we are looking for. This way, as we exposed before, we will not need to link (or even understand) the language implementation. 

First of all the Data Access interface was implemented. At first glance it appeared to be easier to implement than the Control Flow Graph. Eventually this was not so, although the difficulties encountered arose due to some initial design errors.
The implemented algorithm is quite simple. As we said, it's a typical AST visitor. Here we want to track to what type the current variable is being fed. In case it's a reference the variable will be read and written. Otherwise the reference will be read only. With this idea in mind we keep tracking what argument we're in and in the cases the type is built-in by the language we also generate our own parameter types.
For the write-only types like definition and assign this will be done right away so that we can properly provide a valueRange.

The other implemented interface is the Control Flow Graph. The resulting implementation was quite forward -- like the previous one this is an AST visitor. However in this case we: 
\begin{itemize}
  \item Wait to find a code context (either a declaration or a function/method definition) 
  \item There we create the first node in the graph. We walk into the code node recursively and finally we feed it back to the graph together with its declaration if possible.
  \item While walking the expression node we will find different conditional structures such as: \verb|if|, \verb|switch|, \verb|while| or \verb|for|. When we want to create a new node we will proceed as follows: 
  \begin{itemize}
    \item we set the ending cursor of the node. 
    \item we create a new node with the appropriate start cursor. 
    \item we link it to the previous node and proceed the visiting.
  \end{itemize}
\end{itemize}

All this has to be done in a sufficiently reliable way. That's why we created some unit tests for both interfaces. This way we can ensure that everything works as expected. 

The data access interface has a test that provides some code and a list of DataAccess::Flags values. The unit test parses and analyzes the code, then compares the results to what was expected, like in the Control Flow Graph's implementation, but this time we just pass the tested code and the number of nodes. This is already quite a significant check if there have been any regressions. Given that by using this approach we're not taking into account the ranges, when we run the code a graphviz file \cite{graphviz} is generated for every test case so that we can review its consistency ourselves.

% TODO: crear exemples d'ús del control flow graph i data access repository
