//we generate casts first
builtin->insertFunction("SomeClassToParent", /* the downcast */);
builtin->insertFunction("ParentToSomeClass", /* the upcast */);
builtin->insertFunction("ParentIsSomeClass", /* the cast check */);

//we generate class methods
builtin->insertFunction("SomeClass_coolMethod",
                        SomeClass_coolMethod::type(),
                        new SomeClass_coolMethod);
