#include <analitza/analyzer.h>
#include <analitza/expression.h>
#include <iostream>
#include <QtCore/QList>

using namespace Analitza;

class JoinStrings : public FunctionDefinition
{
  virtual Expression operator()(const QList<Expression>& args)
  {
    //we extract the first argument as a list of expression
    QList<Expression> exps = args.first().toExpressionList();
    QStringList strings;
    
    //We turn every expression into a string
    foreach(const Expression& exp, exps)
      strings += exp.stringValue();
    
    //We extract the second argument
    QString joint = args.last().stringValue();
    
    return Expression::constructString(strings.join(joint));
  }
};

int main()
{
  Analyzer a;
  
  //we define the type, in haskell terms:
  //[[char]] -> [char] -> [char]
  ExpressionType joinType(ExpressionType::Lambda);
  joinType.addParameter(
    ExpressionType(ExpressionType::List,
             ExpressionType(ExpressionType::List,
                    ExpressionType(ExpressionType::Char))));
  joinType.addParameter(
    ExpressionType(ExpressionType::List,
             ExpressionType(ExpressionType::Char)));
  //return value
  joinType.addParameter(
    ExpressionType(ExpressionType::List,
             ExpressionType(ExpressionType::Char)));
  
  //we insert the function by passing:
  //the name, the type and the functional object
  a.builtinMethods()->insertFunction("join", joinType,
                                     new JoinStrings);
  
  //join( list{ "john", "peter", "maria" }, ", " )
  Expression exp("join( list{ \"joan\", \"pere\", \"maria\" },"
                  " \", \" )");
  a.setExpression(exp);
  
  /* we proceed just like in the last example */
}