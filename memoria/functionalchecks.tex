\chapter{Functional Checks}
As we discussed on the previous section, we decided to use the KAlgebra project. Here we are going to see what parts are really being used, what parts can be reused, what changes had to be made and what tools were created in order to make it smooth enough.

\section{What is the KAlgebra project?}
The KAlgebra project was initially meant to be just a calculator with plotting capabilities to be used in the KDE Edu\cite{kdeedu} context.

KAlgebra's language is based on MathML, this means that when we're compiling the code instead of providing a regular AST, an XML-based code is provided to be parsed. This has a reason to be, we are not basing our language features to our syntax but to a specification, which eased the initial development. The served as a base to create a fully featured language and started to introduce some functional ideas, like lambda, into the project.

The Analitza module is the part of the KAlgebra project that is able to calculate mathematical expressions. It started as a library to be able to properly define unit tests that make sure that all is working. Soon a lot of useful alternative uses appeared, that's when the console version and the Plasmoid \footnote{Plasma is KDE's desktop environment. A Plasmoid is an applet that can live integrated in the desktop environment.} appeared. Furthermore, Cantor back-end arrived some years later providing a very different and interesting way to use a calculator by integrating it in a styled sheet, like other programs like Maple do. Nowadays there are some other projects using the Analitza module that are work in progress: the KAlgebra Mobile to take some KAlgebra features to mobile devices of different form factors, a new 2D and 3D plotting application yet to be named and an Analitza back-end for Rocs, a KDE Edu application to teach graph theory algorithms.

All in all we can consider Analitza as the library that calculates the things and KAlgebra the global project name as well as the application that started it and that is living in the KDE Edu project.

\section{History}
The KAlgebra project started as a calculator application back in the year 2005, because since in my last years of mathematics studies I wanted to have it. I finally decided to start working with it, when I wanted to investigate a little about source code parsing towards different features like 2D/3D function plotting. I maintained this project since then, with a strong focus on having a feature-full, not-too-flexible language. I still was learning a lot about C++ and software development. It was not the best quality in the beginning but it already started to attract some education-based distributions to distribute it and letting me receive its feedback.

I used that project to put in practice a lot of the skills I acquired in software development, it has been rewritten many times over the years and I have tried to have it as polished as possible so that it can be used as good as possible. It has to be noted that myself, I'm not a KAlgebra user anymore, I don't really need to do this kind of calculations in my day to day life, so my main interest in it was both to adapt it to its actual users needs and to adapt any \textit{cool} feature I could think of, based on my initial premises.

That lead us to the inclusion of the inference type checker, a slow but steady language enhancing and quite some run-time optimization, because we all like fast applications.

\section{Language adaptations}
As I said, even if it made sense to adapt the KAlgebra language, it was missing some very basic features like string support, built-in methods (or bindings if you prefer) and abstracting a little the way we have to import.

\begin{description}
    \item[Type Checker Stabilization] When I started this project the type checker was already in place but it lacked some features regarding the lambda types inference that were needed right when I started to implement high order functions like map and filter, that initially were written in the Analitza language.

    \item[String Support] Until this project there was no need to have String support on the language because it was only used in calculators. The implementation was straight forward. A string is considered a list of characters, so we didn't have to add new semantics. We added the Character primitive and we adapted the parser to understand quoted inputs and we were good to go.

    The fact that they are lists meant that we didn't have to add functions to deal with them, because we could already use the \texttt{union} and \texttt{selector} functions, for instance.

    \item[Built-in Methods] When we decided to go for KAlgebra to get our checks run, the first important thing to do was to implement some way to call code that is not defined inside the scripts, so I created this built-in infrastructure that lets us define code in C++ that can be called from within KAlgebra scripts without needing to put them in the Analitza code. This feature will be described further in the next section.

    \item[Custom Types] One of the most important reasons to use Analitza for the check running was its type checker, but this made evident the requirement to add a new and flexible way to define types. To do so I made it possible to define custom types with a name that can be any string, then the type checker can perform a simple string compare to see if it's the same type or not.

    \item[File Execution] Until now KAlgebra code was mostly run expression by expression from a Graphical User Interface. It was possible to import external files but it was not very optimized for it. Some work was added in order to make it possible to properly parse files with multiple expressions properly.

    \item[Comments] Like in the last case, such a typical feature was not present before in the KAlgebra language. A new token '//' was added that defines where starts and ends the comment.

    \item[New KAlgebra functions] With the new type of coding happening, we got new use cases. Because of the nature of the checking that we're doing, the needs for further boolean functions raised, that's why \textit{exists} and \textit{forall} were added. The high order functions \textit{filter} and \textit{map} were added because its implementation in KAlgebra code were not performing good enough so it was decided to build them inside the language, because they are generic enough.

        Here we can see a detailed list with the changes.
        \begin{description}
            \item[exists] \verb|exists(x : x@list {true, false, false})|, tells if there is any true statement in the bounded value.
            \item[forall] \verb|forall(x : x@list {true, true, true})|, tells if there is all statements in the bounded value are true.
            \item[filter] \verb|filter(x->x>3, list {1,2,3,4})|, will return the elements list that matched the first argument condition.
            \item[map] \verb|map(x->x+3, list {1,2,3,4})|, will apply to all the elements the first argument function and return a list with all the results.
        \end{description}
\end{description}

\section{Using the Analitza module}

\subsection{Getting started}
First of all to read our expression into something that can be computed, we pass the code into an Expression instance that will create a rich AST, to be fed to the Analyzer (or whatever that might want to access the tree, we'll discuss about it later). The Analyzer knows about the variables that we have defined and the different builtin features we might have. Whenever an Expression is tied to it, the type checker will be run so that we can start executing it, which is what we're going for, in the end.

At this point we will have different options, most importantly:

\begin{description}
    \item[calculate]In case there's an unresolved variable it will error, otherwise it will provide a result as fast as possible:
    \item[simplify] Without changing the semantics of the operation, it will modify the Analyzer's expression into an optimized version.
    \item[evaluate] It substitutes variables and simplifies. It's like calculate but instead of calculating, it simplifies to be flexible with the undefined variables.
    \item[calculateLambda] In case it's a lambda function what we have in the expression, it lets us pass the needed arguments with the ``setStack'' method and executes the lambda in function of those. It's useful to calculate functions to be plotted or, like we do in this project, to call a check with the different relevant data.
\end{description}

In the following code snippet we can see how would we invoke some calculation:

\verbatiminput{example1.cpp}

The execution of this code is as simple as expected. This code could be extended to more complex cases, but that's a different issue.

\begin{verbatim}
$ g++ main.cpp -I /usr/include/QtCore/ -l analitza
$ ./a.out
result: 2+sin(pi/2) = 3
\end{verbatim} 

\subsection{Built-in functions}
One of the most important features that we will need in this project is the Built-in Methods. The whole point of using a bindable language is to be able to navigate through some data repository that we can access through some C++ bindings.

To do so, we created some simple infrastructure that lets us create functions given a random name, type and an object that will define what to execute and return. Please note that here it's up to the developer to make sure that the types requested are the ones we're looking for.

Here I wrote a not-so-simple example that shows us how to use this feature. We created a C++ function that given a list of strings and a string returns a string with the list of strings joined by the second argument. The example will join with a coma ``, '' the strings ``joan'', ``pere'', ``maria''.

\verbatiminput{example2.cpp}

The output again, is what we would have expected. A whole string with the list's contents separed with coma's.

\begin{verbatim}
$ g++ main.cpp -I /usr/include/QtCore/ -l analitza
$ ./a.out
result: join(list {"joan", "pere", "maria"}, ", ") = "joan, pere, maria"
\end{verbatim}

This feature doesn't go much further but essentially this, together with the custom types, have made it possible to use the Analitza module to be used in this project all together. This kind of support could be enhanced in many different ways, but for the moment it's been good already.

\section{KAlgebra-based check development}
To understand how does it all fit in our project, let's schematize how does it all work. We have different parts:
\begin{description}
  \item[C++ support] First of all, we have KDevelop's C++ support that, given a C++ file it will generate the different internal data structure we've been describing.

  \item[KDevChecksRunner] There's also the Checks Runner that will figure out when do we need to check a file, ask for the needed data to KDevPlatform's Language support and then it will invoke every found check to be run.

  \item[KDevAnalitzaChecks] This plugin will have is responsible for listing the installed Analitza-based plugins, compile them (while assuming all went well, it won't report errors) and execute them for every file. To see if a check is correct we'll have the \verb|kdevanalitza| \verb|checkcompiler| tool that will print us if there's any error and will provide some data in case we want to debug the application.
\end{description}

\subsection{Binding to the language}
To create the KDevPlatform bindings we needed an automatic way to create a bunch of functions that communicate our scripts to the KDevPlatform, without spending too much time on the task since it could get very repetitive.

\subsubsection{Bindings development}
To do this we created an application that automatically generates the bindings. Firstly, we needed a way to extract information from the C++ class specifications, in the header files, generally. For the parsing we decided to use cpptoxml. It was very convenient because it gets a C++ file and it automatically outputs an XML document. It's really easy to read XML, there's a lot of technology already created for that, and it would help us concentrate on the transformation from an interface to the actual bindings instead of working on reading the C++ header files.

With this, we created a Python \cite{Python} application that: given a header file and which classes we want to extract from it, it extracts the classes definitions in the said XML to be analyzed. From here we will extract all the methods and create an Analitza built-in function from each method. We will also read every enumeration and will create variables for enumeration value we find there.

It should be noted that for every method we will have to check all the arguments and in case it's something that KAlgebra understands we have to translate it to its corresponding KAlgebra type (like in numbers, lists, strings, etc.). If it doesn't exist we can create a KAlgebra custom object value that KAlgebra treats as if it were a black box, which can't be read, but passed around only. Also we will only generate bindings for the methods that don't change the object state, that is \texttt{const} methods and static functions.

With all this, we'll generate a \texttt{bindings.cpp} file that will contain the bindings for all the files. This file will provide a function that takes an \texttt{Analitza::Variables*} and a \texttt{Analitza::BuiltinMethods*} instance and it will fill those with the said bindings.

\subsubsection{Bindings generation example}
After explaining a little how does it all work, let's take a look at what it looks like. First of all, we'll create a simple class header as an example:
\verbatiminput{examplebindings.h}

In this example we can see two classes, an empty Parent one and SomeClass. In this example, SomeClass will be exported to the Analitza facilities to be used from scripts. As discussed before, it's going to be exported by using the \texttt{cpptoxml} tool. Before rushing into the final results, let's see what we're getting here.

\verbatiminput{examplebindings.xml}

As you can see, it's a typical XML structure. To improve the readability of the file, some information was removed for a better understanding by the reader. If the In this file it can be easily seen how it's structured with the classes and the relations specified, now let's see what kind of code do we produce from this.

Here we can see how the arguments are converted from C++ types to KAlgebra specific types by constructing Cn on runtime and Expression::Value as a type.

This first example will be commented for better reading, the following examples will work likewise.

\begin{verbatim}
struct SomeClass_coolMethod : public Analitza::FunctionDefinition
{
  //This function operation will be called when we need
  //this function to be called.
  virtual Expression operator()(const QList<Expression>& args)
  {
    //The first argument is the *this pointer
    SomeClass* thisElement=args[0].customObjectValue()
                                  .value<SomeClass*>();

    //We assert to make sure it's a valid object instance
    Q_ASSERT(thisElement && "calling SomeClass::coolMethod");

    //We extact the actual method arguments
    int arg1=args[1].toReal().intValue();
    
    //We make the call
    double theRet(thisElement->coolMethod(arg1));

    //We return the value in an expression.
    return Expression(Cn(theRet));
  }

  //Here we specify the type for the whole function
  static ExpressionType type() {
     //It's a function
     return ExpressionType(ExpressionType::Lambda)
              //This is the *this argument
              .addParameter(ExpressionType("SomeClass*"))
              //The integer argument
              .addParameter(ExpressionType::Value)
              //The return value
              .addParameter(ExpressionType::Value);
  }
};
\end{verbatim} 

Here we have a simple get method that returns a string. There are two main things to note here: first of all, we can see that we'll still have the \texttt{this} pointer as the first argument, it's maybe evident but worth noting. Also we are returning a string, and we want to be able to read it in our code, so the bindings generator instead of constructing a QVariant\footnote{QVariant is a Qt class that works like a C union that can contain almost-any type or class.} with a QString inside, we'll create a string expression specially constructed by the Analitza module.

\begin{verbatim}
struct SomeClass_getName : public Analitza::FunctionDefinition
{
  virtual Expression operator()(const QList<Expression>& args)
  {
    SomeClass* thisElement=args[0].customObjectValue()
                                  .value<SomeClass*>();
    Q_ASSERT(thisElement && "calling SomeClass::getName");
    QString theRet(thisElement->getName());
    return Expression::constructString(theRet);
  }

  static ExpressionType type() {
    return ExpressionType(ExpressionType::Lambda)
      .addParameter(ExpressionType("SomeClass*"))
      .addParameter(ExpressionType(ExpressionType::List,
                            ExpressionType(ExpressionType::Char)));
  }
};
\end{verbatim} 

Here we are accessing a variable attribute. The are just implementing it like a get method, since we're not letting the checks to modify the code model state. Here it's a number type again, so it will have a \texttt{ExpressionType::Value} type again.

\begin{verbatim}
struct SomeClass_z : public Analitza::FunctionDefinition
{
  virtual Expression operator()(const QList<Expression>& args)
  {
    SomeClass* thisElement=args[0].customObjectValue()
                                  .value<SomeClass*>();
    Q_ASSERT(thisElement && "calling SomeClass::z");
    return Expression(Cn(thisElement->z));
  }

  static ExpressionType type() {
    return ExpressionType(ExpressionType::Lambda)
    .addParameter(ExpressionType("SomeClass*"))
    .addParameter(ExpressionType::Value);
  }
};
\end{verbatim} 

As you have seen, there are just the \texttt{const} methods exported, that way we make sure that the state of the DUChain has not changed. Also it should be noted the presence of the \texttt{Q\_ASSERT} call on every function call. It's useful mostly for debugging situations where you can immediately have an accurate hint about your problem without running the application through a debugger.

\verbatiminput{examplebindings_result.cpp}

Here we can see how the methods have been generated and are passed to the \texttt{BuiltinMethods} instance. I have just put one example of the methods because it's the same every time. First of all we have the casts that we will be able to do to with the type to and from its parent, then we have a list of the functions to be added that first passes the name of the function, then the type and finally an instance of the object that will be called when the function is triggered.

\subsubsection{Problems with the binding generator}
Having a tool that generates the code you need automatically is good, but making sure that it all works properly can be tricky sometimes. During the development of this tool we have had to make sure all the typing is coherent in every case: is \texttt{RangeInRevision} different to \texttt{KDevel::RangeInRevision}? Our language doesn't have name-space abstractions so we have to make sure we use the same nomenclature everywhere.

In addition, \texttt{cpptoxml} tool is not perfect, there are some problems that are being workaround in our code. It's not ideal but sometimes we need just to deliver. Another problem was that some include headers are not prepared to be used with compilers other than \texttt{gcc} so we must try to create an environment that is the closest to it, if we don't want to fall in some \textit{pre-processor traps} like the one that was calling \texttt{\#define const} in some \texttt{\#ifndef} clauses. It can get complex to debug those issues.

\subsection{Developing an Analitza-based check}

The check development definition is quite simple: we receive some input data that we want to pass to every check to deal with and in exchange we return a list of problems. When starting to work on checks, I found out that a pattern emerged quite automatically, that's why I defined this function:

\begin{verbatim}
abstractCheck := (problemConstructor, isCorrect, elements) ->
                  map(problemConstructor,
                      filter(inc->not(isCorrect inc), elements))
\end{verbatim} 

This function defines the steps that we will do in most checks:
\begin{enumerate}
  \item Extract a list of elements to be checked. These elements can be of any type, depends on what we are checking.
  \item Define a function that given one of those elements tells if it's correct or not.
  \item Given an incorrect element, construct a problem out of it.
\end{enumerate}

The logic of the \texttt{abstractCheck} function states that the first step will provide the elements, those elements will be filtered with a negated call to the second step function and the resulting element list will be mapped to the problem constructor to be given back to KDevelop, that will register it into the code model. It should be noted here that the check is not modifying the state of the checks data, just reading it and providing results out of it.

To give a better taste of how do these functional checks work, I'll show a stripped-down version of a simple one.

\verbatiminput{example.kal}

This check will look up all the declarations in a file and figure out if there's any use of it. If there isn't, a new problem will be reported. This check can be improved in many ways but it's useful because it's compact enough. This example only relies on the DUChain to provide any results, but all the data structures are available to the language and are used for other checks.

\subsection{The Analitza Check Compiler tool}
As discussed earlier, the possibility to know if the check is well built is very important and we want to make full use of it. If we think about it, given the check's code and all the bindings' definitions we will be able to know whether its check has coherent typification. A simple (yet powerful) tool was created for that.

The main reason of the application is that we will run the application with some \texttt{*.kdevcheck} files as an argument and we'll have the errors being displayed (if any). If there are not we will be able to start trying them in the \texttt{kdevchecker} tool, otherwise there's no reason (actually it will lead the KDevAnalitzaChecks plugin to crash due to asserting about the check correction).

There are some other useful features:
\begin{itemize}
  \item The \texttt{--doc <file.html>} argument lets us generate an HTML file with some list of facilities provided by the bindings.
  \item The \texttt{--variables} argument will list all the defined variables with its value and type.
\end{itemize}

It's very important to have this kind of tools, it lets us split our development process into what you should do to make the check to just work from sketching what you're looking for in the first place. As said previously in this document, in my case the type checking already solved me some problems regarding type coherence without even running the program. That saves up a lot of time debugging code that's just not correct.