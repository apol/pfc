#include <analitza/analyzer.h>
#include <analitza/expression.h>
#include <iostream>

int main()
{
  Analitza::Expression exp("2+sin(pi/2)");
  Analitza::Analyzer a;
  a.setExpression(exp);
  
  Analitza::Expression ret = a.calculate();

  if(a.isCorrect())
    std::cout << "result: "
      << exp.toString().toStdString()
      << " = "
      << ret.toString().toStdString()
      << std::endl;
  else
    std::cout << "errors:\n - "
      << a.errors().join("\n - ").toStdString
      << std::endl;
  
  return 0;
}

