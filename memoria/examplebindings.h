#include <QtCore/QString>

class Parent {};

class SomeClass : public Parent
{
public:
	double coolMethod(int a) const;
	
	QString getName() const;

	//Won't be exported because it's not const
	void setName(const QString& name);
	
	//some attributes
	int z;
};
