\chapter{Checks}
Like previously said, this project will provide a set of checks that, on the one hand, will show us how to make use of the facilities provided by this project and, on the other, will start to provide some features that we will be able to benefit from in our projects.

All these checks have been developed using the technologies created in this project, mainly the new data structures and the language that was adapted especially for this purpose, as evidence that it works fine and also that it's useful to create any kind of check.

These checks are not thought to be means to demonstrate that your project is going to work. They are targeting more at properly using the language's features, like proper \texttt{const}-ness and the removal of unused code, like in the imports, where we're telling the user that he's importing code that is not needed or the constant condition check that will tell the user that he wrote some code that won't be run.

\section{Imports Check}
This is the first check that was implemented using this infrastructure. It was decided to start with this one because it depends on the DUChain only, which is convenient as a first approach.

This one gathers all the Declarations used in the top context (that don't come from the same context) and checks if there's any import importing it. It's a good check because it helps to keep track of which imports are good and which ones aren't.

Note that in this context \textit{import} would translate, in the C++ world, into an \texttt{\#include} of another module.

\begin{figure}[h]
  \includegraphics[height=7cm]{./importscheck.pdf}
  \centering
  \caption{Imports Check's dependencies graph.}
\end{figure}

In the last figure we can see more or less how has this check been organized. It's structure is very flat with the check procedure taking up most of the work.

Here what we do is, first of all, to extract a list of the files that are being imported. We'll provide those to the \texttt{isCorrectImport} call that, together with the results of \texttt{neededDeclarationsContext} that will tell us the non-local declarations used in the file, will check the import by seeing if there's any of those declarations provided by the import. If there isn't then it's not a correct import and it should be removed, so we create an error for it.

Let's see in which cases it would work and why:

\begin{verbatim}
#include <string>

int main()
{}
\end{verbatim} 

As discussed above, here we will be checking first of all, what imports it has: in this case just the one, afterwards the code will be scanned for all the relevant used declarations. Once we know them all, we will check if any of the declarations provided by the import is being used in the file. In this case there isn't any declaration using the string import, therefore it can be removed, so a problem will be registered.

\begin{verbatim}
#include <string>

int main()
{
  std::string s;
}
\end{verbatim}

In this case, it will work the same way. The big difference here is that we have a \verb|std::string| use. When we are checking the \verb|std::string| declaration against the string import, it will tell us that the import is being used, meaning that it will be a valid import and no problem will be created.

I'd like to note here that the use of \verb|std::| or \verb|using namespace std;| is not relevant. These checks are run after the semantic analysis, so checks don't really worry about it.

\section{Const Method Check}
This check is relying on DUChain and Data Access. It tells us if any class method is not modifying any class attribute and in this case it adds an error telling that the \texttt{const} is missing.

I think it's an important check. If we want people to follow good coding practices such as the \texttt{const} method decoration, if developers use it safer code will be produced by making sure not to have side-effects. What a better way to enforce its use, than by pointing out what methods are missing it automatically, so that they can be changed?

\begin{figure}[h]
  \includegraphics[width=\textwidth]{./constmethodcheck.pdf}
  \centering
  \caption{Const Method Check's dependencies graph.}
\end{figure}

This check is a little more complicated to analyze, let's go step by step. We want to know if any method declarations should be \texttt{const}. First of all, we extract all the function declarations and we filter the class methods from the rest (functions and static methods don't have attribute members so there's no need).

Among those methods, we first check if it's already \texttt{const}, if it is, then it's ok, if it isn't we check all the variable accesses; to see if there's member being modified and the function calls to see if they are non-\texttt{const} class methods being called. If there are class writes or non-\texttt{const} calls, we create a problem for it.

Let's see how it would behave. 

\begin{verbatim}
class A {
  int b;

public:
  int f() { return b; }
};
\end{verbatim}

In this case, the f method should be \texttt{const}, because we are not modifying any class field or calling a non-\texttt{const} method. First of all, the check will find all the methods, it will filter the ones that are already \texttt{const} and then it will analyze the contents of the function. If there's nothing inside that would prevent the method to be \texttt{const}, then it should be. In this case we are just reading the variable \textit{b} so it should be, therefore a problem will be created asking the user to add the keyword.

\begin{verbatim}
class A {
  int b;

public:
  void setB(int b) { b=3; }
};
\end{verbatim}

This one is correct already. The function won't be filtered out because it's a method, so it will proceed by checking inside the function's body. There we will see that there is indeed a write to a class member. In this case a problem shall not be created.

\begin{verbatim}
class A {
  void f(int) const;

public:
  void g(int b) { f(b+3); }
};
\end{verbatim}

Calling methods will work similar. In this case \textit{g} should be \texttt{const} because it's just reading arguments and calling other constant methods, so it can be turned to \texttt{const} as well. In this case the code will find all the methods, filter the ones that are interesting to us because they can be turned, then we will find what's in the methods we're interested in. In this case just reading arguments and calling other \texttt{const} methods from the same class instance, therefore this is a problem to be notified.

\section{Unused Declaration Check}
This check will be using only the DUChain and it probably has the simplest logic among the provided checks, but not for being simple it's less useful. It will tell us if either a variable, a class, a function or whatever we have indexed as a declaration doesn't have a use. This one will help us to clean up our code from useless declarations.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{./unuseddeclarationcheck.pdf}
  \centering
  \caption{Unused Declaration Check's dependencies graph.}
\end{figure}

In this check, first of all, we'll extract all the declarations in the file. For every declaration we'll ask the DUChain whether it has any use, if it doesn't a problem will be created telling us not to declare variables just because.

\begin{verbatim}
int f() {
  int a=0;
  return 3;
}
\end{verbatim}

In this case, there are 2 declarations: \textit{f} and \textit{a}. None of them has any use at all, so two problems will be created, one for each symbol, displaying that nobody is using them and maybe they can be removed.

\begin{verbatim}
int f() {
  int a=0;
  return a;
}
\end{verbatim}

In this case, the variable \textit{a} will have a use, so this one won't generate an error. In this case the check will only generate a problem for the \textit{f} function.

\begin{verbatim}
class C {};
\end{verbatim}

In this case, we will look up for uses of the C declaration. The strategy here is to parse all the .cpp files before we parse the header files, this means that we will have all the possible uses in the DUChain already and we will be able to provide accurate checks so that when we check it, we'll get all the uses in the project that use the class \textit{C}.

\section{Constant Condition Check}
This check will make a semantic analysis of the conditions in the code by using all the DUChain, the Data Access and the Control Flow Graph. It will check if there's a case where we can tell that the conditional value is always constant, if this is happening a problem will be reported. It's interesting to have this check because it lets you remove code that otherwise could have been cluttering our project, but that has no sense or meaning at all but the increased complexity.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{./constantconditioncheck.pdf}
  \centering
  \caption{Constant Condition Check's dependencies graph.}
\end{figure}

% TODO: Descriure una mica el graf

When this check is being run, first of all we'll extract all the conditional nodes in the code and from those we will extract their ranges. From these ranges it will check where does the data come from: if it's a function call, then it's not constant\footnote{we're not taking into account constant functions for the moment, the standard c++11 is too recent.}, if it's coming from variables we will keep checking where is the value coming from and repeat the same procedure until we know if it's just a constant. To know where the value is coming from, we will consider all the different uses of the variable and make sure there's a path from the use to the place we are checking.

We will create a problem in case there's just one value option for every the value in the conditional.

\begin{verbatim}
bool a=false;

if(a) {
  //Very complicated code
}
\end{verbatim}

In this code, first of all we obtain the range for the \texttt{if} condition, then we analyze the uses in its range. These uses can be, either a function or variable use. In case it's a function we won't continue the search, but given that it's a variable we'll go check the variable \textit{a}'s value. In this case, when we analyze the value definition, we won't find any function call or different kinds of value that tells us it's not a constant, therefore this will mean it's a constant value.

\begin{verbatim}
bool a=false;

if(a) {
  //Very complicated code
}

a=true;

\end{verbatim} 

In this case we will also have two uses of \textit{a}: one at the definition and the other after the conditional. In this case we will check the same cases as the first because there's no path from the use after the condition to the condition, so a problem will be reported again.

\begin{verbatim}
bool a=false;

while(a) {
  //Very complicated code

  a=true;
}
\end{verbatim}

In this case we will proceed like in the example above. We'll check the condition, which will find a use of \texttt{a}. In this case we'll walk all the a writes again and we'll find out that there are two places where the variable is set that have a path to the condition, which will render evident that it's not a problematic case, because they can have different values.

We should note here that the part that decides is the possible paths counting, not the value itself. We just know if it's a constant value or if it comes from somewhere else.

\section{Undefined Read Check}
One of the most important limitations of the imperative programming languages is all the network of undefined states we can get to. One of the most important cases of those is the undefined reads. They can make a program not to work just because the algorithm is using some value with undefined value. Left aside the discussion regarding if the language should allow it or not, here we're proposing a check that will point out all these problems.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{./uninit.pdf}
  \centering
  \caption{Undefined Read Check's dependencies graph.}
\end{figure}

To find undefined reads, what we will do is to start by extracting all data accesses and filter the reads. Once we have all the reads in the program we will extract the declaration of the accesses to figure out if it could be a problem, for instance classes have constructors, hence they don't need to be initialized explicitly, also non-local variables won't be taken into account either. When we have made sure it's a correct option to consider, it will start looking for the last write for every read. If there's no write in any of the paths, we'll mark it as an undefined read.

% TODO: Explicar graph

% TODO: Exemples
\begin{verbatim}
int f()
{
  int a;
  return a;
}
\end{verbatim}

In this first example, we would find just 1 data access: A read on \textit{a}. What would happen here is that we would get that data access, we would make sure it's a local variable and an integral data type, so it could be considered. There's no write data access in the only path from the declaration to the function return.

\begin{verbatim}
int f()
{
  int a=1;
  return a;
}
\end{verbatim}

This example is similar to the previous one, but here the \textit{a} declaration is being defined. This means that we will have a write data access in addition to the return's. First of all we will discard the non-read data accesses, so we will just analyze the return one and there we will find out there's a write data access on a before the read, so we won't trigger the create problem process.

\begin{verbatim}
int f(int x)
{
  return x;
}
\end{verbatim}

In this case there is a read data access that is being return. If we analyzed it we might find out it's not being defined, this won't happen because it will be filtered out for not being defined inside the function, so no problem will be added.

\section{Checks execution}
Before finishing this chapter I'd like to wrap it up a little by discussing what's the outcome of all of this. There is many data that we can contrast to decide if those checks are useful and which ones should be created further. Also there's the false positives, there's the usefulness of the checks, etc. There's a lot of data that is being processed all the time, here I'll put some together and analyze it to have an overview about how did the checks behave and draw some conclusions.

In this section, will discuss the results of these checks on the Analitza code, which is a medium-to-small project, C++ mostly and with a lot of Qt constructions, which can be hard to analyze, specially when there are pre-processor macros being called.

\begin{figure}[h!]
  \includegraphics[width=400px]{./stats1.png}
  \centering
  \caption{Problems per check}
\end{figure}

First of all I wanted to check what is the ratio of the problems found on the Analitza code. I didn't remove the errors first, the reason for that is that I'm interested in analyzing code that is supposed to work but we're running the checks on top. 

Here we can see the results we're having. As I said some Qt features can be problematic when running checks, in this case the QtTest module uses class run-time knowledge to call the tested methods, this increases already the problems count. Something similar happens with the \texttt{const} method check, where a lot of those test methods will be marked as errors while they aren't, also there are some other methods that should be \texttt{const} and they aren't. C++ Templates also trigger some false positives, but in this case we probably should adapt our checks better to these slightly-\textit{weird} C++ constructions.

Furthermore, I was interested in how does the complexity vary depending on the different checks we have created. One of the concerns that came up when deciding to go for KAlgebra for our functional checks back-end was that it could not be as performing as other implementations. Of course here we don't have data to compare but we can share the results of what we have.

\begin{figure}[h!]
  \includegraphics[width=400px]{./stats2.png}
  \centering
  \caption{Elapsed time per check}
\end{figure}

In this graph first of all I'd like to note that there's no specific payload for running the checks on our functional infrastructure. Also I'd like to note that the less performing checks are the ones that rely on sophisticated calculations the most. We should note here that the KAlgebra run-time hasn't been optimized for the use case here, so there's some room for improvement, but that is out of scope here. Another thing I'd like to note is that there are some checks that take notably more time to run than others, so it would be probably a good idea to classify them by payload so, for example, some are just run when the project is batch-analyzed but not when the developer is coding.

Another thing I wanted to try is how does the source code size impact on the execution time. There would have been different ways to check that, I chose to count by file size over file execution time. The reason is that this way we are not skewed by any premise but for what we have, if we assume that all the code-base analyzed. One could argue that it would be more accurate to calculate it over tokens, variable definitions or source code lines, but I don't think it would display with enough fidelity what I'm looking for: how the run-time grows with source code size.

\begin{figure}[h!]
  \includegraphics[width=400px]{./stats3.png}
  \centering
  \caption{Run time on file size}
\end{figure}

At a first glance on the results I saw that most of the C++ header files had considerably less run-time than the implementation files. This makes sense, because the ConstantConditionCheck and the UninitializedReadCheck won't have any effect there, if the implementations are in the .cpp files, so the ConstMethodCheck will have less work there too. That said, in the graph we can see there's a quite linear relation. The more the file grows, the longer it takes. The worst case is the analyzer.cpp file that takes 4.5s to run, but has around 5000 characters in it, so it's roughly at the same distribution line, if not a little lower.

All in all, my conclusion here is that it's not cheap to completely analyze a file, because it ranges from 500ms to 1500ms, but it's still feasible in a small amount of time which can also be diminished by reducing the amount of checks being run. Also results show us a too high amount of false positives that should be considered and taken into account properly when creating future checks.

\subsection{Future}
Ironically, whenever we try to leave something as done, we can't but think of what we can do to take it further. On one hand it would be really interesting to investigate on how can we make the run-time faster. There are many possibilities to explore, JIT code generation, making use of the type checking for the run-time, etc.

Also there's a lot of new checks to be written: double writes, API checks in STL or Qt, code style checks and a big list of items that we couldn't finish. Quality Assurance is an interesting field of research and we can use checks as a tool to find out what can we do to improve the overall development experience and help the code and projects in general to be maintainable.