\documentclass[a4paper,10pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage{url}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{fullpage} %margins

%opening
\title{Static Analysis on Imperative Languages}
\author{Aleix Pol Gonzàlez\\
  Tutor: José Miguel Rivero Almeida
}

\begin{document}

\maketitle

\begin{abstract}
In this document we describe what work that is being done in order to add static analysis capabilities to a language platform like KDevelop's architecture and how did we make a working project out of it.
These static capabilities will have in mind the regular imperative and object oriented languages and will use C++ as the provided implementation.
\end{abstract}

\section{Introduction}
The main idea about this project is to create a framework that will make it possible to analyze source code and report relevant information the best possible way. To do so, we have separed the project in 3 main parts: the data recollection, the check development and some ways to present the conclusions to the user. Likewise this document has been prepared the same way.

This project, even if it could be adapted to many languages, is centered in analyzing C++ because, on one hand, it's an important and widespread language that could take advantage of the project right away and, on the other hand, we have a working implementation of it in KDevelop, which narrows down our work to extending the analysis process to actually do what we're looking for, saving us the non-trivial task of designing and developing a C++ parser.

\subsection{Why static analysis?}
When I was thinking about choosing a subject for this project I knew I wanted to work on programming languages. Moreover I wanted to create something that would help developers create better software. My KDevelop background helped me decide to start creating some tools that would end up in this framework creation.

First of all I wanted to start a project that would take advantage of the amazing KDevPlatform infrastructure to create checks. There is few static analysis tools in the Free Software world and I figured out we could make a difference there.

Knowing that made me decide to create a tool that will find some determined problems in random software projects. To do so we started to create the checking infrastructure and I began to work on creating checks that tell the most simplistic problems that the compiler generally won't.

We want to achieve, by using this project, simplicity regarding the check development. It should be really easy to create new checks that make sure that trivial problems are not happening and support some of the not-so-trivial ones.

\subsection{The Starting Point}
As we have mentioned before, this project is largely built on top of the KDevelop project which was already working when this project started. In KDevelop we could find some abstractions already created, like the plugin infrastructure or facilities to ask the platform to parse a project and tell the language support to parse all its files. What we did was to add the necessary changes so that we would get the files parsed the way we want them to (by default KDevelop does some minimal parsing on the files that are not being edited, which is not really enough for us) and of course added the new interfaces and provide them from the C++ support.

\subsection{Goals}
To sum up, here we have the following main goals for this project:
\begin{itemize}
  \item To abstract the language support from the check creation. The check developer doesn't have to be responsible for modifying AST\footnote{Stands for Abstract Syntax Tree, it's the code representation provided by the parser.} visitors to check the information, but gather the data once and have it provided.
  \item To provide a set of checks as minimal features and as a proof of concept.
  \item To provide a set of tools to display the checks findings in a user friendly way.
\end{itemize}

\section{Architecture and Data Collection}
First of all, to be able to end up in conclusions regarding some code, we'll extract the data we need and we'll put it in abstract interfaces. An alternative to that is to provide the raw AST, but I believe that this way we'll be able to get more powerful checks with less effort. For the moment, we'll be providing 3 main data sources: the DUChain, a Control Flow Graph and information about the access for every variable in the code. With this information I believe that a wide range of problems can be addressed already.

Regarding the DUChain, even though it's not provided by this project (it was in KDevPlatform already) I'll try to explain it briefly so that the user understands what is it used for. DUChain stands for Declaration-Use Chain and its main purpose is to create a structured database with all the code symbols and its relevant information. The idea behind it in the beginning, was to provide a solid base to create code completion and code navigation on top of the raw parser, but it has proved to be a very useful information repository that can be useful in different cases. In the DUChain we'll store any variable or type declaration together with its type and uses. That makes it possible to analyze it and find out many useful information. More information about DUChain can be found in KDevPlatform's DUChain Documentation. \cite{apikdeorg}

\subsection{Interfaces}
In the following section I'll explain the interfaces that have been created for both: accessing and constructing the information. I won't explain the design of the DUChain because it falls out of the scope of this project.

\subsection{Data Access}
This interface will tell us, for every variable, if it's being read, modified or both. By variable here, we mean an identifier representing some data. We will be able to extract where the data comes from by accessing to the position's Declaration or Use.

\subsubsection{Use cases}
The use case for the data access data structure is pretty simple. Specially, in the case of imperative languages (the ones treated by this project), it's very useful to understand where we are accessing data and modifying it. It's a very common problem if we're adding code that changes the value semantics in unneeded ways. We need to be able to know where values are coming from and what is being changed. Together with the Control Flow Graph we can manage to know how will the program evolve in the future.

\subsection{Control Flow Graph}
The other data interface that we have introduced is the Control Flow Graph. This one is used to create a representation that tells us how is the code going to behave considering a current state by specifying which are the different code paths and what are the parts that tell who decide what path to take.

\begin{multicols}{2}
\begin{verbatim}
int f(int a)
{
  if(a)
    a+=1;
  else
    a-=1;
  return a+3;
}
\end{verbatim}

\includegraphics[width=5cm]{controlflow.eps}
\end{multicols} 

Here we can see the output\footnote{As discussed later, this graph is automatically generated by the unit tests} of the code placed in the left in form of a control flow graph on the figure at the right. We can see that instead of having the code in a form of text array we can see the relationships regarding the code flow.

\subsubsection{Use cases}
If we want to be able to analyze some code base we need to know what is the code going to react to and where is it leading to. The Control Flow Graph tackles that by providing us the network of possibilities that are happening in it.
A typical use case example would be the constant condition problem, where we get to statically tell that a condition is always going to jump to the same place, which would mean it's an unneeded condition and can be removed.

\section{Checking Infrastructure}
Once we have all the data we have to start thinking about what a check is and how far do we want to take them. The KDevPlatform semantics are rich enough regarding problem generation but we want to define what we're aiming for, so that we can act consequently in the future. Furthermore, we don't want to cut out any possibilities, so we tried to make a flexible interface that fits our specification but we don't make sure it's used entirely the way we expect it to, at least for the moment.

Given the scope of this project, we can consider a check as some program that, given some data, adds problems information to the project, tying it to its file.

\section{Check Execution}
Now we have reviewed our check architecture so we need different pieces to make it possible to take full advantage of the provided checks. In this chapter we'll explain the tools that have been created in order to do so.

% ...

\subsection{Running Checks}
\subsubsection{Editor integration}
The first chosen approach was to create a KDevelop plugin, mainly for sake of simplicity. In KDevelop we already have most the environment logic prepared: we are importing the projects, we are parsing the project's files, we have places in the GUI where to show the found problems, so in this case what we just did was to prepare a plugin that hooks to the KDevelop GUI and performs there the checking when it's due. 
This means we are getting, automatically, the checks being run while developing KDevelop. It's not our final use case but I think it's a very interesting feature on its own already.

\subsubsection{Project checking}
We want to have also a standalone application that parses and analyzes a given project and then it provides a report with all problems found in the project. 

\subsection{Check Distribution}
When starting to develop checks, the first thing that occurred to me in order to start developing checks, was to start the checks pack. Here we have a plugin that cares about the infrastructure by providing every check inside the pack, which will be in a separate .cpp file that statically registers itself to the plugin to be initialized when it's required.

More on the subject will follow regarding how we can improve the check distribution in a general and efficient way.

\section{Creating checks}
When we were talking about the projects goals, we said that one of the important parts is to make it easy to create further checks. In this chapter we'll explain what is the proposed way to create checks, what do we expect the developer to do and what will the developer have to worry about. Along this document we have specified how to create a big and wide range of opportunities for the check developers, here we'll try to narrow these possibilities down in favor of better flexibility.

\subsection{C++ checks}
Creating C++ checks is a very good option if we want to be aware of how the check code will be executed when it's working on a computer but that is a very specific case and it's not always what we're looking for. Sometimes we will want to favor ease of distribution or code simplicity instead of run-time efficiency, that's why we have created this section where we will discuss the advantages and problems of every option.

\subsection{Scripted Checks}
C++ checks are great. There we can use the KDevPlatform default library, we can be very specific about what do we want from the check to do so that it's efficient and we can do whatever we feel like with it, it's a KDevelop plugin and our only limitation is our creativity. On other hand we found some limitations that we decided that are worth addressing.

First of all in the C++ code we're defining how to check something but, in a way, we're going for what to check. This means that sometimes writing those checks can be a little hard in C++. In addition there's the distribution problem, a C++ check has to be compiled before installing and, given the different places where it can be run, it's probably a good idea to be able to have some alternative, this will be discussed properly in the final memory of this project.

\section{Checks}
As we said, this project will provide a set of checks that will show us, in one hand, how to take advantage of the facilities provided by this project and, in the other, will start to provide some features that we will be able to use in our projects.

All these checks have been developed using the technologies created in this project, mainly the new data structures and the language that was adapted specially for it, as a proof that it works fine and that it's useful to create any kind of check.

\subsection{Imports}
This is the first check that was implemented using this infrastructure. It was decided to start with this one because it's just depending on the DUChain, which is convenient as a first approach.
This one gathers all the Declarations used in the top context (that don't come from the same context) and checks if there's any import importing it. It's a good check because it helps to keep track of what imports are good and what imports aren't.

\subsection{Const Method Check}
This check is also relying on DUChain only and it tells us if any class method is not modifying any class attribute and in this case it adds an error telling that the \texttt{const} is missing.

\subsection{Unused Declaration Check}
For every declaration in the file we're checking if there's any use of it (with some practical exceptions) and if there isn't we report it.

\subsection{Constant condition}
Checks every data access done in a conditional expression and makes sure it's not going to have always the same result (if possible).

\section{Project State}
At the moment, most of the project is in place. The interfaces have been created and added to the KDevPlatform, we can generate both the Data Access repositories and the Control Flow Graph and we have the mentioned checks working, mostly.

There is still work to be done:
\begin{itemize}
  \item Improve the problem reporting UI.
  \item Testing with big projects, making sure it doesn't crash and scales to medium projects.
  \item Study usefulness of the created checks.
  \item Create some still missing tests like: double write and undefined read, these will be explained when they are ready in the final report.
\end{itemize}

Most of this work can be found in the KDE\cite{kde} repositories:
\begin{itemize}
  \item KDevPlatform clone with the modifications.
    \url{http://gitweb.kde.org/?p=clones/kdevplatform/apol/kdevplatform-pfc.git}
  \item KDevelop clone with the modifications.
    \url{http://gitweb.kde.org/?p=clones/kdevelop/apol/kdevelop-pfc.git}
  \item KDevChecksRunner. The plugin responsible for the check invocation.
    \url{http://gitweb.kde.org/?p=scratch/apol/kdevchecksrunner.git}
  \item KDevChecksPack. A pack for C++ checks.
    \url{http://gitweb.kde.org/?p=scratch/apol/kdevcheckspack.git}
  \item KDevAnalitzaChecks. A plugin that provides the bindings to create Analitza checks (the language discussed before)
    \url{http://gitweb.kde.org/?p=scratch/apol/kdevanalitzachecks.git}
\end{itemize}

In these URL's you'll find some websites that will tell you how to retrieve the code. In case you want to try it out, you'll have to compile it like regular KDevelop. Instructions can be found here: \url{http://techbase.kde.org/KDevelop4/HowToCompile}, of course by using the repositories provided by this project.


\begin{thebibliography}{9}
\bibitem{apikdeorg} KDevPlatform DUChain Documentation, available at
  \url{http://api.kde.org/extragear-api/kdevelop-apidocs/}.
\bibitem{kde} The KDE community
  \url{http://kde.org}.
\end{thebibliography}

\end{document}
  